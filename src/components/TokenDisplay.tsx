import {Token} from '../tokenizer';
import Ruby from './Ruby';

type TokenDisplayProps = {
    token: Token;
}

export default function TokenDisplay(props: TokenDisplayProps) {

    if (props.token.furigana) {
        return <Ruby kanji={props.token.kanji} furigana={props.token.furigana}/>;
    }

    return props.token.kanji;

}
