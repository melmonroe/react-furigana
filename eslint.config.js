import pluginJs from '@eslint/js';
import pluginImport from 'eslint-plugin-import';
import pluginReact from 'eslint-plugin-react';
import globals from 'globals';
import tseslint from 'typescript-eslint';

export default [
    {files: ['**/*.{js,mjs,cjs,ts,jsx,tsx}']},
    {languageOptions: {globals: globals.browser}},
    pluginJs.configs.recommended,
    ...tseslint.configs.recommended,
    pluginReact.configs.flat.recommended,
    {
        plugins: {
            import: pluginImport
        },
        rules: {
            'react/react-in-jsx-scope': 'off',
            // Simple quotes for Js/Ts
            'quotes': ['error', 'single', {'avoidEscape': true}],
            // Double quotes for Jsx/Tsx
            'jsx-quotes': ['error', 'prefer-double'],
            // No extensions in imports
            'import/extensions': ['error', 'never'],
        },
        settings: {
            react: {
                version: 'detect'
            },
            'import/resolver': {
                node: {
                    // No extensions in imports only for these extensions
                    extensions: ['.js', '.jsx', '.ts', '.tsx']
                }
            }
        }
    }
];
